﻿using System;
using EDO.Core.ViewModel;
using EDO.Main;
using System.Collections.ObjectModel;
using EDO.Core.Model.OptionalPropertyModel;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace EDO.OptionalPropertiesCategory.OptionalPropertiesForm {
    /// <summary>
    /// 任意設定項目設定画面のViewModelです。
    /// </summary>
    public class OptionalPropertiesFormVM : FormVM {
        public OptionalPropertiesFormVM(StudyUnitVM studyUnit, OptionalPropertyDefinitions optionalPropertyDefinitions) : base(studyUnit) {
            var tabIndex = 1;
            OptionalPropertyVMs =
                studyUnit.OptionalPropertyModels
                .Select(m => {
                    var vm = new OptionalPropertyVM(m, optionalPropertyDefinitions.Definitions.Find(d => d.Key == m.Key), tabIndex++);
                    vm.PropertyChanged += OptionalPropertyChanged;
                    return vm;
                })
                .ToLookup(vm => vm.Definition)
                .ToDictionary(group => group.Key, group => group.ToList());
            OptionalPropertyDefinitions = optionalPropertyDefinitions;
        }

        public Dictionary<OptionalPropertyDefinition, List<OptionalPropertyVM>> OptionalPropertyVMs { get; private set; }
        public OptionalPropertyDefinitions OptionalPropertyDefinitions { get; }

        public void OptionalPropertyChanged(object sender, PropertyChangedEventArgs args) {
            Memorize();
        }

        public class OptionalPropertyVM : INotifyPropertyChanged {
            private readonly OptionalProperty _Property;
            private readonly OptionalPropertyDefinition _Definition;
            public OptionalPropertyDefinition Definition { get => _Definition; }

            public int TabIndex { get; private set; }

            public string Key { get => _Property.Key; }

            public string Description { get => _Definition.Description; }

            public string Value {
                get => _Property.Value;
                set {
                    if (value == _Property.Value)
                        return;
                    _Property.Value = value;
                    RaisePropertyChanged();
                }
            }

            public bool Multiline { get => Definition.Type == OptionalPropertyType.LongText; }

            public int MinHeight { get => Multiline ? 40 : 20; }

            public OptionalPropertyVM(
                OptionalProperty optionalProperty,
                OptionalPropertyDefinition optionalPropertyDefinition,
                int tabIndex) {
                _Property = optionalProperty;
                _Definition = optionalPropertyDefinition;
                TabIndex = tabIndex;
            }

            public event PropertyChangedEventHandler PropertyChanged;
            private void RaisePropertyChanged([CallerMemberName]string propertyName = null)
            => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        }
    }
}
