#define MyAppName 'Easy DDI Organizer'
#define MyAppVersion GetFileVersion('..\EDO\bin\Release\EDO.exe')
#define MyAppProductVersion GetStringFileInfo('..\EDO\bin\Release\EDO.exe', 'ProductVersion')
#define MyAppPublisher 'SSJDA'

[Setup]
OutputDir=out
SourceDir=.
AppendDefaultDirName = no
AppName={#MyAppName}
AppVerName={#MyAppName} {#MyAppProductVersion}
AppVersion={#MyAppVersion}
VersionInfoVersion={#MyAppVersion}
VersionInfoProductTextVersion={#MyAppProductVersion}
OutputBaseFilename=EDO-{#MyAppProductVersion}
AppPublisher={#MyAppPublisher}
AppCopyright={#MyAppPublisher}
DefaultDirName={pf}\{#MyAppName}
DefaultGroupName={#MyAppName}
UninstallDisplayIcon={app}\EDO.exe
Compression=lzma2
SolidCompression=yes

; downloading and installing dependencies will only work if the memo/ready page is enabled (default and current behaviour)
DisableReadyPage=no
DisableReadyMemo=no

; supported languages
#include "scripts\lang\english.iss"
#include "scripts\lang\japanese.iss"

[Files]
Source: "..\EDO\bin\Release\EDO.exe"; DestDir: "{app}"
Source: "..\EDO\bin\Release\EDO.exe.config"; DestDir: "{app}"
Source: "..\EDO\bin\Release\DocumentFormat.OpenXml.dll"; DestDir: "{app}"
Source: "..\EDO\bin\Release\SpssLib.dll"; DestDir: "{app}"
Source: "..\EDO\bin\Release\WPFLocalization.dll"; DestDir: "{app}"
Source: "..\EDO\bin\Release\en\EDO.resources.dll"; DestDir: "{app}\en"
Source: "..\EDO\DDI\*"; Destdir: "{app}\DDI"; Flags: recursesubdirs

[Icons]
Name: "{group}\Easy DDI Organizer"; Filename: "{app}\EDO.exe"
Name: "{group}\Uninstall Easy DDI Organizer"; Filename: "{uninstallexe}"

[Run]
Filename: "{app}\EDO.exe"; Description: "Easy DDI Organizer���N��"; Flags: nowait postinstall skipifsilent

[CustomMessages]
DependenciesDir=MyProgramDependencies

#include "scripts\products.iss"

#include "scripts\products\stringversion.iss"
#include "scripts\products\winversion.iss"
#include "scripts\products\fileversion.iss"
#include "scripts\products\dotnetfxversion.iss"

#include "scripts\products\dotnetfx47.iss"

[Code]
function InitializeSetup(): boolean;
begin
  dotnetfx47(70);
  Result := true;
end;


