﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace EDO.Core.Model.OptionalPropertyModel {

    /// <summary>
    /// StudyUnitに保存される任意設定項目の定義です。項目の値は同じKeyを持つ<see cref="EDO.Core.Model.OptionalPropertyModel.OptionalProperty"/>で保存します。
    /// </summary>
    public class OptionalPropertyDefinition {

        /// <summary>
        /// 任意設定項目を特定するためのKeyです。
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// 任意設定項目の概要です。
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 任意設定項目の型が<see cref="EDO.Core.Model.OptionalPropertyModel.OptionalPropertyType.List"/>であった場合、リストの要素を定義します。
        /// </summary>
        [XmlArrayItem("Item")]
        public List<string> List { get; set; }

        private bool _Multiple;
        /// <summary>
        /// 同じKeyを持つ任意設定項目が複数存在可能かどうか（つまり配列かどうか）を表します。
        /// </summary>
        public bool Multiple {
            get { return _Multiple; }
            set { _Multiple = value; }
        }

        /// <summary>
        /// 任意設定項目の型です。
        /// </summary>
        public OptionalPropertyType Type { get; set; }
    }
}