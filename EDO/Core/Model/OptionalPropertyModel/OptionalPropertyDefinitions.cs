﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace EDO.Core.Model.OptionalPropertyModel {

    /// <summary>
    /// 任意設定項目定義（<see cref="EDO.Core.Model.OptionalPropertyModel.OptionalPropertyDefinition"/>）のコンテナクラスです。
    /// 定義ファイルの作成者や作成日時など、メタデータはここに実装します。
    /// </summary>
    public class OptionalPropertyDefinitions {
        [XmlArrayItem("Definition")]
        public List<OptionalPropertyDefinition> Definitions { get; set; }
    }
}