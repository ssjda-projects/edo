﻿using EDO.Core.Util;
using EDO.Core.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EDO.OptionalPropertiesCategory.OptionalPropertiesForm {
    /// <summary>
    /// OptionalPropertiesFormView.xaml の相互作用ロジック
    /// </summary>
    public partial class OptionalPropertiesFormView : FormView {
        public OptionalPropertiesFormView() {
            InitializeComponent();
        }

        private OptionalPropertiesFormVM VM {
            get {
                return EDOUtils.GetVM<OptionalPropertiesFormVM>(this);
            }
        }
    }
}
