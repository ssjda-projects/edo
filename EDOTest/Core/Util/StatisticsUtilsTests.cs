﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using EDO.Core.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EDO.Core.Util.Tests {
    [TestClass()]
    public class StatisticsUtilsTests {
        [TestMethod()]
        public void ToCategoryValueTest() {
            var result = StatisticsUtils.ToCategoryValue(123.0d);
            Assert.AreEqual("123", result);
            result = StatisticsUtils.ToCategoryValue(-123.0d);
            Assert.AreEqual("-123", result);
            result = StatisticsUtils.ToCategoryValue("123.0");
            Assert.AreEqual("123", result);
            result = StatisticsUtils.ToCategoryValue("-123.0");
            Assert.AreEqual("-123", result);
            result = StatisticsUtils.ToCategoryValue(123);
            Assert.AreEqual("123", result);
            result = StatisticsUtils.ToCategoryValue(-123);
            Assert.AreEqual("-123", result);
        }
    }
}