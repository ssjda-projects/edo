#Easy DDI Organizer
[日本語](./Readme_ja.md)

Easy DDI Organizer\(EDO\) is an application for survey planning and metadata management. EDO enables you to plan social surveys as well as prepare metadata about the survey compliant to Data Documentation Initiative \(DDI\). You can export various documents used in your survey \(e.g. questionnaire, codebook\).


##System Requirement
OS: Windows 7 / Windows 10 \(recommended)

.Net Framework: .NET Framework 4.7\(full\)

##Licenses
Apache License v2.0

EDO bundles redistributable libraries and icons from other projects below. 
Original Licenses are applied to each library/ icon.

* Libraries
  - [SPSS & PSPP file library](http://spsslib.codeplex.com/) \(MIT License\)
  - [Simple WPF Localization](http://www.codeproject.com/Articles/30035/Simple-WPF-Localization) (Code Project Open License)
  - [DDI 3.1 XML Schema](http://www.ddialliance.org/Specification/DDI-Lifecycle/3.1/) (GNU Lesser General Public License version 3 or later)
  - [DDI 2.5 XML Schema](http://www.ddialliance.org/Specification/DDI-Codebook/2.5/) (GNU Lesser General Public License version 3 or later)

* Icons
  - [Farm-Fresh Web Icons](http://www.fatcow.com/free-icons)(Creative Commons Attribution 3.0 License)

##Acknowledgement
This project is funded by Joint usage/ research center project of the Ministry of Education, Culture, Sports, Science and Technology, Japan.

##History
 - [This project was previously developed on another web site.](https://github.com/Easy-DDI-Organizer/EDO)