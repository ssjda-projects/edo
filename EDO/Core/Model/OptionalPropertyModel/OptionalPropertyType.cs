﻿namespace EDO.Core.Model.OptionalPropertyModel {
    public enum OptionalPropertyType {
        Text,
        LongText,
        Number,
        Date,
        List
    }
}