﻿namespace EDO.Core.Model.OptionalPropertyModel {

    /// <summary>
    /// StudyUnitに保存される任意設定項目です。項目の定義は同じKeyを持つ<see cref="EDO.Core.Model.OptionalPropertyModel.OptionalPropertyDefinition"/>で行われます。
    /// </summary>
    public partial class OptionalProperty {

        /// <summary>
        /// 任意設定項目を特定するためのKeyです。
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// 任意設定項目の値です。
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// 任意設定項目の型です。
        /// </summary>
        public OptionalPropertyType Type { get; set; }

    }
}