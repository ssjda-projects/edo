﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using EDO.Core.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EDO.Core.Model.OptionalPropertyModel;
using System.Xml.Serialization;
using System.IO;

namespace EDO.Core.IO.Tests {
    [TestClass()]
    public class EDOSerializerTests {
        [TestMethod()]
        public void LoadOptionalPropertyDefinitionsTest() {
            var result = EDOSerializer.LoadOptionalPropertyDefinitions("Fixtures/OptionalPropertyDefinitions.xml");
            Assert.AreEqual(result.Definitions.Count(), 3);
            Assert.AreEqual(result.Definitions[0].Key, "property 1");
            Assert.AreEqual(result.Definitions[0].Description, "sample property");
            Assert.AreEqual(result.Definitions[0].Type, OptionalPropertyType.Text);
            Assert.IsTrue(result.Definitions[0].Multiple);
            Assert.IsFalse(result.Definitions[1].Multiple);
            Assert.AreEqual(result.Definitions[2].List[0].Trim(), "item 1");
        }

        [TestMethod()]
        public void LoadBadFormatOptionalPropertyDefinitionsTest() {
            var result = EDOSerializer.LoadOptionalPropertyDefinitions("Fixtures/OptionalPropertyDefinitions_BadFormat.xml");
            Assert.IsNull(result);
        }
    }
}